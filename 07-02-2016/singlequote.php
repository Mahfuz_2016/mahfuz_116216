<?php
    echo 'this is a simple string';
    echo 'Youcan also have embedded newlines ins strings this way as it is okay to do';
    // outputs: Arnold once said: "I'll be back"
    echo 'Arnold once said: "I\'ll be back"';
    //outputs: You deleted c:\*.*?
    echo 'you deleted c:\\*.*?';
    //outputs: You deleted c:\*.*?
    echo 'You deleted c:\*.*?';
    //outputs: Variables do not $expand $either
    echo 'Variables do not $expand $either';
?>

