<?php

	//addslashes section 
	$str="this is your name O'really";
	echo addslashes($str). "</br></br>";
	
	//explode section
$sohel = "Mohakhali Dhaka Bangladesh";
$shahin = explode(" ", $sohel);
echo $shahin[0]. "</br>";
echo $shahin[1]. "</br>";
echo $shahin[2]. "</br>";
echo "</br>";


//join section

$sohel = array(1,2,3,4,5);
echo join('-', $sohel);

echo "</br></br>";


//implode section
$sohel = array('sohel','shahin','aziz','we are friends');
$shahin = implode(",",$sohel);
echo $shahin;
echo "</br></br>";

// or implode section
$sohel = array('sohel','shahin','aziz','we are friends');
echo implode(",", $sohel);
echo "</br></br>";


//htmlentities section
$sohel = "<p>This is a htmlentities example <b>cods</b></p>";
echo htmlentities($sohel);

echo "</br></br>";


//nl2br section
echo nl2br("This is a nl2br\n example cods");

echo "</br></br>";

echo nl2br("This is a nl2br\r example cods");

echo "</br></br>";

echo nl2br("This is a nl2br\r\n example cods");

echo "</br></br>";

echo nl2br("This is a nl2br\n\r example cods");

echo "</br></br>";

//str_pad section

$str = "SOHEL";
echo str_pad($str, 10). "</br>";
echo str_pad($str, 10, "+"). "</br>";
echo str_pad($str, 10, "+", STR_PAD_LEFT)."</br>";
echo str_pad($str, 10, "+", STR_PAD_RIGHT)."</br>";
echo str_pad($str, 10, "+", STR_PAD_BOTH)."</br>";

echo "</br></br>";

//str_repeat section
echo str_repeat("Sohel Rana </br>", 5);

echo "</br></br>";

//or str_repeat section
echo str_repeat("Sohel Rana\n", 5);

echo "</br></br>";

//str_split section

$str = "Hello world";
$str1 = str_split($str);
$str2 = str_split($str, 6); //6 or any digit you can use just flow bootstrap grid system thanks.

print_r($str);
echo "</br>";
print_r($str1);
echo "</br>";
print_r($str2);


echo "</br></br>";

//strip_tags section

$sohel = "<p>This is strip_tags test code <a href='#'>for example</a></p>";
echo strip_tags($sohel);
echo strip_tags($sohel,'<p><a>');
	
echo "</br></br>";

//strlen section 

$str = "Sohel";
echo strlen($str);
echo "</br>";

$str = " Sohel Rana ";
echo strlen($str);


echo "</br></br>";

//strtolower case section
$str = "THIS IS STRTOLOWER CASE TEST CODE FOR LEARNING";
echo strtolower($str);

echo "</br></br>";

//or strtolower case section
$str = "THIS IS STRTOLOWER CASE TEST CODE FOR LEARNING";
$str = strtolower($str);
echo $str;

echo "</br></br>";

//strtoupper section
$str = "this is strtoupper case code for learning";
echo strtoupper($str);




echo "</br></br>";

//ucfirst section
$str = "this is a ucfirst test code for learning";
$str = ucfirst($str);
echo $str;

echo "</br></br>";

//or ucfirst section
$str = "this is a ucfirst test code for learning";
echo ucfirst($str);

echo "</br></br>";

//or ucfirst section
$str = "THIS IS UCFIRST TEST CODE FOR LEARNING";
echo ucfirst(strtolower($str));

echo "</br></br>";

	
?>